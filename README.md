# vimrc

My entire Vim setup, split into vimrc and contents of $HOME/.vim

## Mappings

### C/C++

| Mapping | Mode | Effects | Comments |
| ------- | ---- | ------- | -------- |
|\<Leader\>i | Normal | Include and sort header files | |
|\<Leader\>g | Normal | Insert header guard | |
|\<Leader\>n | Normal | Insert namespace    | C++ only |
| Ctrl-j     | Normal | Insert right-angle brackets ('>') matching number of unclosed left-angle brackets | C++ only |
| Ctrl-j     | Insert | Insert right-angle brackets ('>') matching number of unclosed left-angle brackets | C++ only |

### Assembly

| Mapping | Mode | Effects |
| ------- | ---- | ------- |
|\<Leader\>i | Normal | Insert function, with accompanying comments |
|\<Leader\>p | Normal | Push and pop register(s) for duration of function |

### Latex

| Mapping | Mode | Effects | Comments |
| ------- | ---- | ------- | -------- |
|\<Leader\>ia | Normal | Insert algorithm | |
|\<Leader\>ib | Normal | Insert bmatrix | |
|\<Leader\>ie | Normal | Insert equation | |
|\<Leader\>if | Normal | Insert figure | |
|\<Leader\>it | Normal | Insert tikz picture | Assumes \\usepackage{tikz} is specified |
|\<Leader\>m  | Normal | Launches [texwatch](https://gitlab.com/vilhelmengstrom/texwatch) via Makefile | Assumes the Makefile has a target called 'watch' |
|\<Leader\>n  | Normal | Reset texwatch | Assumes the Makefile has targets 'watch' and 'clean' |
|\<Leader\>ta | Normal | Toggle alternate math delimiters | |
| $           | Insert | Insert \\( or \\) (alternating) | If alternate math delimiters is on |
| $$          | Insert | Insert $  | If alternate math delimiters is on |

## Installing

The [setup](setup) script symlinks the required files and directories in $HOME to the contents of this repository. Any files or directories that would be overwritten are moved to FILENAME.old, e.g. .vimrc -> .vimrc.old, before doing so.
```bash
    ./setup
```

