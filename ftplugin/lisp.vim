nnoremap <silent> <Leader>m :call RunLispScript() <CR>

function RunLispScript()
    if &filetype != 'lisp'
        call EchoError('Not a Lisp buffer')
        return
    endif
    let clisp_invoke = 'clisp -q '
    let l:clisprc = $HOME . '/.clipsrc.lisp'
    if filereadable(l:clisprc)
        let clisp_invoke = clisp_invoke . '-i ' l:clisprc
    endif

    call ShellCmdInVerticalScratchBuffer(clisp_invoke . expand('%'))
endfunction
