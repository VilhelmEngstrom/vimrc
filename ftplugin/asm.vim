nnoremap <silent> <Leader>i :call InsertAsmFunction() <CR>
nnoremap <silent> <Leader>p :call PushRegs() <CR>

function PushRegs()
    let l:volatile_registers = ['rbp', 'rsp', 'rbx', 'r12', 'r13', 'r14', 'r15']
    let l:save = winsaveview()
    let reglist = split(substitute(input('Registers to push: '), '\v(,| )+', ',', 'g'), ',')

    if !len(reglist)
        call EchoError('No registers specified')
        call winrestview(l:save)
        return
    endif

    if len(reglist) == 1 && get(reglist, 0) == '*'
        let reglist = l:volatile_registers
    else
        for l:reg in reglist
            if index(l:volatile_registers, l:reg) == -1
                if l:reg == '*'
                    call EchoError('* may not be specified along other registers')
                else
                    call EchoError(l:reg . ' is not a volatile register')
                endif
                call winrestview(l:save)
                return
            endif
        endfor
    endif

    let l:endfunc = search('ret', 'Wc')

    if !l:endfunc
        call EchoError('No function below cursor position')
        call winrestview(l:save)
        return
    endif

    let l:startfunc = search('\v^[a-zA-Z].*:\s*$', 'Wbc')

    if !l:startfunc
        call EchoError('Could not find global label above `ret` on line ' . l:endfunc)
        call winrestview(l:save)
        return
    endif

    let l:pushlist = PrependInstructionToList(reglist, "\tpush")
    let l:poplist = reverse(PrependInstructionToList(reglist, "\tpop"))

    call append(l:endfunc - 1, l:poplist)
    call append(l:startfunc, l:pushlist)

    call cursor(l:startfunc + len(l:pushlist) + 1, 0)
endfunction

function PrependInstructionToList(reglist, instruction)
    let list = []
    for l:reg in a:reglist
        call add(list, a:instruction . ' ' . l:reg)
    endfor

    retur list
endfunction

function InsertAsmFunction()
    let l:paramregs = ['rdi', 'rsi', 'rdx', 'rcx', 'r8', 'r9']
    let l:save = winsaveview()
    let l:funcname = input('Function name: ')
    let nparams = trim(input('Number of params: '))

    if !len(l:funcname)
        call EchoError('No function name specified')
        call winrestview(l:save)
        return
    endif

    if !len(nparams)
        let nparams = 0
    elseif nparams !~# '^\d\+$'
        call EchoError(nparams . ' is not a number')
        call winrestview(l:save)
        return
    endif

    let insertlist = ['; TODO: description', '; Params:']
    if nparams == 0
        call add(insertlist, ';     -')
        let nparams += 1
    else
        if nparams > len(l:paramregs)
            call EchoWarning('Warning: too many parameters, some have to be passed on the stack')
            let nparams = len(l:paramregs)
        endif
        let i = 0
        while i < nparams
            call add(insertlist, ';   ' . get(l:paramregs, i) . ': TODO')
            let i += 1
        endwhile
    endif

    let insertlist = insertlist + ['; Return:', ';   eax: TODO', l:funcname . ':', '', '    ret']

    let l:lnum = line('.')

    if LineIsEmpty(getline('.'))
        call setline(l:lnum, get(insertlist, 0))
        call append(l:lnum, Tail(insertlist))
        call cursor(l:lnum + 5 + nparams, 0)
    else
        call append(l:lnum, insertlist)
        call cursor(l:lnum + 6 + nparams, 0)
    endif
endfunction
