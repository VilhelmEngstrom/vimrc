function GetNumOpenBuffers()
    return len(getbufinfo({'buflisted':1}))
endfunction

function GetBufferTypes()
    let typelist = []

    let i = 1
    let l:numbufs = GetNumOpenBuffers()
    while i <= l:numbufs
        call add(typelist, getbufvar(i, '&filetype'))
        let i += 1
    endwhile

    return typelist
endfunction

function GetNumTypePendingBuffers()
    let l:typelist = GetBufferTypes()

    let num = 0
    for l:type in l:typelist
        if l:type == ''
            let num += 1
        endif
    endfor

    return num
endfunction
