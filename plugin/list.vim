function Tail(list)
    let result = []

    let i = 1
    let l:length = len(a:list)
    while i < l:length
        call add(result, get(a:list, i))
        let i += 1
    endwhile

    return result
endfunction

function GetItemsMatchingPattern(list, pattern)
    let result = []

    for l:item in a:list
        if match(l:item, a:pattern) != -1
            call add(result, l:item)
        endif
    endfor

    return result
endfunction

function SubstituteListItems(list, pattern, repl, flags)
    let result = []

    for l:item in a:list
        call add(result, substitute(l:item, a:pattern, a:repl, a:flags))
    endfor

    return result
endfunction

function FilterList(list, pattern)
    let result = []

    for l:item in a:list
        if match(l:item, a:pattern) != -1
            call add(result, l:item)
        endif
    endfor

    return result
endfunction

function ListMaxStrlen(list)
    let i = 1
    let max = strlen(get(a:list, 0))

    while i < len(a:list)
        let l:len = strlen(get(a:list, i))
        if l:len > max
            let max = l:len
        endif
        let i += 1
    endwhile

    return max
endfunction
