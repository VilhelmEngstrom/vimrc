function EchoMsg(msg)
    echon "\r\r"
    echon a:msg
endfunction

function EchoError(msg)
    echon "\r\r"
    echohl ErrorMsg
    echon a:msg
    echohl None
endfunction

function EchoWarning(msg)
    echon "\r\r"
    echohl WarningMsg
    echon a:msg
    echohl None
endfunction

function StripTrailingWhitespace()
    let l:save = winsaveview()
    if !&binary && !&filetype != 'diff'
        %s/\v\s+$/\2/e
        call EchoMsg('Trailing whitespace stripped')
    endif
    call winrestview(l:save)
endfunction

function StripTrailingWhitespaceExcludingFiletypes(typelist)
    if !count(a:typelist, &filetype)
        call StripTrailingWhitespace()
    endif
endfunction

function ListDirContents()
    let l:contents = systemlist("ls -l | tail -n +2 | awk '{sub(/-/, \" \", $1)} 1 {print $1 \" \" $9}'")
    let l:entries = sort(SubstituteListItems(l:contents, '\v^(.).*\s([^ ]+$)', '\1    "\2"', 'g'))
    let l:maxfnumlen = strlen(len(l:entries))
    let fnum = 1
    echom ':!ls'
    for l:entry in l:entries
        echom repeat(' ', 2 + l:maxfnumlen - strlen(fnum)) . fnum . ' ' . l:entry
        let fnum += 1
    endfor
endfunction
