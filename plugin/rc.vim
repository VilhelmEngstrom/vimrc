function IsMyVimrc(file)
    let l:canon_vimrc_path = trim(system('readlink -f ' . $MYVIMRC))
    let l:canon_file_path = trim(system('readlink -f ' . a:file))
    return l:canon_vimrc_path == l:canon_file_path
endfunction
